﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ABM.DAL;
using ABM.Entities;
using ABM.Models;

namespace ABM.Controllers
{
    public class BillController : Controller
    {
        ////Creating One Client:
        //var NewClient = new Client();
        //NewClient.Name = "Martin";
        //NewClient.ID = 1;
        ////Creating One Product:
        //var NewProduct = new Product();
        //NewProduct.Name = "Tornillo";
        //NewProduct.ID = 1;

        ////Creating One BillDetail
        //var NewBillDetail = new BillDetail();
        //NewBillDetail.Amount = 1;
        //NewBillDetail.Price = 1;
        //NewBillDetail.Total = NewBillDetail.Amount * NewBillDetail.Price;
        //NewBillDetail.Product = NewProduct;
        //NewBillDetail.ID = 1;


        ////Creating One Bill:
        //var NewBill = new Bill();
        //NewBill.Client = NewClient;
        //NewBill.Date = DateTime.Now;
        //NewBill.ID = 1;
        //var NewListBillDetail = new List<BillDetail>();
        //NewListBillDetail.Add(NewBillDetail);
        //NewBill.BillDetail = NewListBillDetail;

        //Conection to DB for acces to the tables
        public Context ConnectDB = new Context();
       

        // GET: Bills
        public ActionResult Index()
        {

            //First Load
            //var NewListToShow = new List<Bill>();
            //var ModelToSet = new Bill();

            //var ModelToSet2 = new Bill();

            //var ListToShowClients = new List<Client>();

            //foreach (var item in ListDBCLients)
            //{
            //    var ModelClient2 = new Client();
            //    ModelClient2.Name = item.Name;
            //    ModelClient2.ID = item.ID;
            //    ModelToSet2.Client = ModelClient2;
            //    ListToShowClients.Add(ModelToSet2.Client);

            //}

            //foreach (var item in NewListOfTableBills)
            //{


            //    var ModelClient = new Client();
            //    ModelClient.Name = item.Client.Name;
            //    ModelClient.ID = item.Client.ID;
            //    ModelToSet.Client = ModelClient;
            //    ModelToSet.Date = item.Date;
            //    ModelToSet.ID = item.ID;


            //    var ModelBilldetail = new BillDetail();
            //    ModelBilldetail.Amount = item.BillDetail.Amount;
            //    ModelBilldetail.ID = item.BillDetail.ID;
            //    ModelBilldetail.Price = item.BillDetail.Price;
            //    var ModelProduct = new Product();
            //    ModelProduct.Name = item.BillDetail.Product.Name;
            //    ModelProduct.ID = item.BillDetail.Product.ID;
            //    ModelBilldetail.Product = ModelProduct;
            //    ModelBilldetail.Total = item.BillDetail.Total;
            //    ModelToSet.BillDetail = new List<BillDetail>();
            //    ModelToSet.BillDetail.Add(ModelBilldetail);


            //    NewListToShow.Add(ModelToSet);
            //}

            var NewListOfTableBills = ConnectDB.Bills.ToList();
            var ListDBCLients = ConnectDB.Clients.ToList();
            var ListDBBillsDetails = ConnectDB.BillsDetails.ToList();
            var ListDBProducts = ConnectDB.Products.ToList();

            var NewListToShow = new List<Bill>();
            var ModelToSet = new Bill();

            var ListToShowClients = new List<Client>();

            foreach (var item in NewListOfTableBills)
            {
                var ModelClient = new Client();
                ModelClient.Name = item.Client.Name;
                ModelClient.ID = item.Client.ID;
                ModelToSet.Client = ModelClient;
                ModelToSet.Date = item.Date;
                ModelToSet.ID = item.ID;

                foreach (var item2 in item.BillDetail)
                {
                    var ModelBilldetail = new BillDetail();
                    ModelBilldetail.Amount = item2.Amount;
                    ModelBilldetail.ID = item2.ID;
                    ModelBilldetail.Price = item2.Price;
                    ModelBilldetail.Product = new Product();
                    ModelBilldetail.Product.Name = item2.Product.Name;
                    ModelBilldetail.Product.ID = item2.Product.ID;
                    ModelBilldetail.Total = item2.Total;
                    ModelToSet.BillDetail = new List<BillDetail>();
                    ModelToSet.BillDetail.Add(ModelBilldetail); 
                }
                
              
                //me guarda siempre el mismo nombre

                NewListToShow.Add(ModelToSet);
            }



            return View(NewListToShow);
        }
        public ActionResult Clients()
        {
            var ListClients = ConnectDB.Clients.ToList();
            var ListClientsToShow = new List<Client>();
            foreach (var item in ListClients)
            {
                var ClientModel = new Client();
                ClientModel.Name = item.Name;
                ClientModel.ID = item.ID;

                ListClientsToShow.Add(ClientModel);
            }
            return View(ListClientsToShow);
        }
        public ActionResult NewClient()
        {
            
            return View();
        }
        public ActionResult NewProduct()
        {
            return View();

        }
        public ActionResult Products()
        {

            var ListProducts = ConnectDB.Products.ToList();
            var ListProductsToShow = new List<Product>();
            foreach (var item in ListProducts)
            {
                var ProductModel = new Product();
                ProductModel.Name = item.Name;
                ProductModel.ID = item.ID;

                ListProductsToShow.Add(ProductModel);
            }
            return View(ListProductsToShow);
           
        }
        public ActionResult ClientEdit(int ID)
        {

            var ClientFromDB = ConnectDB.Clients.Where(x => x.ID == ID).ToList();
            var ClientToEdit = new Client();
            
            foreach (var item in ClientFromDB)
            {
                ClientToEdit.Name = item.Name;
                ClientToEdit.ID = item.ID;
            }

            
            
            return View(ClientToEdit);
        }
        public ActionResult ClientDelete(int ID)
        {
            var ClientFromDB = ConnectDB.Clients.Where(x => x.ID == ID).ToList();
            var ClientToDelete = new Client();

            foreach (var item in ClientFromDB)
            {
                ClientToDelete.Name = item.Name;
                ClientToDelete.ID = item.ID;
            }



            return View(ClientToDelete);
          
        }
        public ActionResult ProductEdit(int ID)
        {

            var ProductFromDB = ConnectDB.Products.Where(x => x.ID == ID).ToList();
            var ProductToEdit = new Product();

            foreach (var item in ProductFromDB)
            {
                ProductToEdit.Name = item.Name;
                ProductToEdit.ID = item.ID;
            }



            return View(ProductToEdit);
        }
        public ActionResult ProductDelete(int ID)
        {
            var ProductFromDB = ConnectDB.Products.Where(x => x.ID == ID).ToList();
            var ProductToDelete = new Product();

            foreach (var item in ProductFromDB)
            {
                ProductToDelete.Name = item.Name;
                ProductToDelete.ID = item.ID;
            }



            return View(ProductToDelete);

        }

        public ActionResult NewBill()
        {

            var ListClients = ConnectDB.Clients.ToList();
            var ListClientsToShow = new List<Client>();
            foreach (var item in ListClients)
            {
                var ClientModel = new Client();
                ClientModel.Name = item.Name;
                ClientModel.ID = item.ID;

                ListClientsToShow.Add(ClientModel);
            }
            return View(ListClientsToShow);
        }
        public ActionResult CreateBill(int ID)
        {
            //Guardado de cliente
            var ClientFromDB = ConnectDB.Clients.Where(x => x.ID == ID).ToList();
            var NewClient = new Client();
            foreach (var item in ClientFromDB)
            {
                NewClient.Name = item.Name;
                NewClient.ID = item.ID;
            }


            ViewData["ClientName"] = NewClient.Name;
            ViewData["ClientID"] = NewClient.ID;

            var ListProducts = ConnectDB.Products.ToList();
            var ListProductsToShow = new List<Product>();
            foreach (var item in ListProducts)
            {
                var ProductModel = new Product();
                ProductModel.Name = item.Name;
                ProductModel.ID = item.ID;

                ListProductsToShow.Add(ProductModel);
            }



            return View(ListProductsToShow);
        }


        [HttpPost]
        public ActionResult NewClient(string NewClient)
        {
            var AddClient = new Clients();
            AddClient.Name = NewClient;

            ConnectDB.Clients.Add(AddClient);
            ConnectDB.SaveChanges();
            return RedirectToAction ("Clients");
        }
        [HttpPost]
        public ActionResult NewProduct(string NewProduct)
        {
            var AddProduct = new Products();
            AddProduct.Name = NewProduct;

            ConnectDB.Products.Add(AddProduct);
            ConnectDB.SaveChanges();
            return RedirectToAction("Products");
        }
        [HttpPost]
        public ActionResult ClientEdit(Client Client)
        {

            var ClientFromDB = ConnectDB.Clients.FirstOrDefault(x => x.ID == Client.ID);
            ClientFromDB.Name = Client.Name;

            ConnectDB.SaveChanges();
            return RedirectToAction("Clients");
        }
        [HttpPost]
        public ActionResult ClientDelete(Client Client)
        {

            var ClientFromDB = ConnectDB.Clients.FirstOrDefault(x => x.ID == Client.ID);
            ConnectDB.Clients.Remove(ClientFromDB);
            

            ConnectDB.SaveChanges();
            return RedirectToAction("Clients");
        }
        [HttpPost]
        public ActionResult ProductEdit(Product Product)
        {

            var ProductFromDB = ConnectDB.Products.FirstOrDefault(x => x.ID == Product.ID);
            ProductFromDB.Name = Product.Name;

            ConnectDB.SaveChanges();
            return RedirectToAction("Products");
        }
        [HttpPost]
        public ActionResult ProductDelete(Product Product)
        {

            var ProductFromDB = ConnectDB.Products.FirstOrDefault(x => x.ID == Product.ID);
            ConnectDB.Products.Remove(ProductFromDB);


            ConnectDB.SaveChanges();
            return RedirectToAction("Products");
        }
        [HttpPost]
        public ActionResult BillInProcess(Bills Factura)
        {
            
            //Creating One Bill:
            var NewBill = new Bills();
            NewBill.Client = Factura.Client;
            NewBill.Date = Factura.Date.Date;
            NewBill.ID = Factura.ID;
            NewBill.BillDetail = new List<BillsDetails>();
            NewBill.BillDetail.AddRange(Factura.BillDetail);
         
            ConnectDB.Bills.Add(NewBill);
            ConnectDB.SaveChanges();
            
            return RedirectToAction("Index");
        }
    }
}