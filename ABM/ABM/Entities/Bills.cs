﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ABM.Entities;

namespace ABM.Entities
{
    public class Bills
    {
        public int ID { get; set; }
        public DateTime Date { get; set; }
        public Clients Client { get; set; }
        public List<BillsDetails> BillDetail { get; set; }
        



    }
}