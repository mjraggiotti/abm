﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ABM.Models;
using ABM.Entities;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ABM.DAL
{
    public class Context : DbContext
    {
        public  Context() : base("Context")
        {
        }
        public DbSet<Bills> Bills { get; set; }
        public DbSet<BillsDetails> BillsDetails { get; set; }
        public DbSet<Clients> Clients { get; set; }
        public DbSet<Products> Products { get; set; }
    }
}