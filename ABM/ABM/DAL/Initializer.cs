﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using ABM.Models;
using ABM.Entities;

namespace ABM.DAL
{
    public class Initializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<Context>
    {
        protected override void Seed( Context context)
        {

            //Creating One Client:
            var NewClient = new Clients();
            NewClient.Name = "Martin";
            NewClient.ID = 1;
            context.Clients.Add(NewClient);
            context.SaveChanges();

            //Creating One Product:
            var NewProduct = new Products();
            NewProduct.Name = "Tornillo";
            NewProduct.ID = 1;
            context.Products.Add(NewProduct);
            context.SaveChanges();

            //Creating One BillDetail
            var NewBillDetail = new BillsDetails();
            NewBillDetail.Amount = 2;
            NewBillDetail.Price = 3;
            NewBillDetail.Total = NewBillDetail.Amount * NewBillDetail.Price;
            NewBillDetail.Product = NewProduct;
            NewBillDetail.ID = 1;
            context.BillsDetails.Add(NewBillDetail);
            context.SaveChanges();

            //Creating One Bill:
            var NewBill = new Bills();
            NewBill.ID = 1;
            NewBill.Client = NewClient;
            NewBill.Date = DateTime.Now;
            NewBill.BillDetail = new List<BillsDetails>();
            NewBill.BillDetail.Add(NewBillDetail);
            context.Bills.Add(NewBill);
            context.SaveChanges();

        }
    }
}