﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ABM.Entities;

namespace ABM.Models
{
    public class Bill
    {
        public int ID { get; set; }
        public DateTime Date { get; set; }
        public Client Client { get; set; }
        public List<BillDetail> BillDetail { get; set; }
    }
}