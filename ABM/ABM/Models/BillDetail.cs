﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ABM.Entities;

namespace ABM.Models
{
    public class BillDetail
    {
        public int ID { get; set; }
        public int Amount { get; set; }
        public double Price { get; set; }
        public double Total { get; set; }
        public Product Product { get; set; }
    }
}